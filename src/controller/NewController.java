package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.IndexModel;

import org.apache.lucene.queryparser.classic.ParseException;

import view.SearchBar;
import view.AppGUI;

public class NewController extends Observable implements ActionListener {

	private IndexModel model;

	public NewController(IndexModel model) {
		this.model = model;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		switch (command) {
		case "About App":
			aboutApp();
			break;
		case "Close":
			closeMainViewer();
			break;
		case "Search":
			try {
				model.getSearchQuery(((SearchBar) e.getSource()).getText());
			} catch (IOException | ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		default:
			functionNotImpl();
			break;
		}
	}

	private void aboutApp() {
		JOptionPane.showMessageDialog(new JFrame(),
				"Implemented by: Group 8\n\t\t Version " + AppGUI.VERSION,
				"CS412", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(
						"Img/folder.jpg"));
	}

	private void closeMainViewer() {
		System.exit(0);
	}

	private void functionNotImpl() {
		JOptionPane.showMessageDialog(new JFrame(),
				"Function Not Implemented!", "FNI",
				JOptionPane.INFORMATION_MESSAGE,
				new ImageIcon("Img/folder.jpg"));
	}
}
