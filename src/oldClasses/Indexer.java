package oldClasses;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.jsoup.Jsoup;

public class Indexer {

	public Indexer() {
	}

	public static void index(String docDirectory, String indexDirectory,
			boolean update) throws IOException {
		// Check If Directories can be used.
		if ((!usableDir("Index", indexDirectory))
				|| (!usableDir("Document", docDirectory)))
			System.exit(1);

		Date start = new Date();
		System.out.println("Indexing to directory '" + indexDirectory + "'...");

		Directory dir = FSDirectory.open(new File(indexDirectory));

		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_45);
		IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_45,
				analyzer);

		if (update) { // Do we create new index or add to an existing one
			iwc.setOpenMode(OpenMode.CREATE);
		} else {
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}
		IndexWriter writer = new IndexWriter(dir, iwc);
		indexDocs(writer, new File(docDirectory));

		// iwc.setRAMBufferSizeMB(256.0); // To Improve Speed -Xmx512m or -Xmx1g
		// writer.forceMerge(1); //Improve Search Performance

		writer.close();

		System.out.println(new Date().getTime() - start.getTime()
				+ " total milliseconds");

	}

	private static boolean usableDir(String dirType, String docDir) {
		if (!new File(docDir).exists() || !new File(docDir).canRead()) {
			System.out
					.println(dirType
							+ " directory '"
							+ docDir
							+ "' does not exist or is not readable, please check the path");
			return false;
		}
		return true;
	}

	private static void indexDocs(IndexWriter writer, File file)
			throws IOException {
		// do not try to index files that cannot be read
		if (file.canRead() && file.isDirectory()) {
			String[] files = file.list();
			// an IO error could occur
			if (files != null) {
				for (int i = 0; i < files.length; i++) {
					indexDocs(writer, new File(file, files[i]));
				}
			}
		} else {

			FileInputStream fis = new FileInputStream(file);

			try {

				// make a new, empty document
				Document doc = new Document();

				// Add the path of the file as a field named "path". Use a
				// field that is indexed (i.e. searchable), but don't
				// tokenize
				// the field into separate words and don't index term
				// frequency
				// or positional information:
				Field pathField = new StringField("path", file.getPath(),
						Field.Store.YES);
				doc.add(pathField);

				// Add the last modified date of the file a field named
				// "modified".
				// Use a LongField that is indexed (i.e. efficiently
				// filterable with
				// NumericRangeFilter). This indexes to milli-second
				// resolution, which
				// is often too fine. You could instead create a number
				// based on
				// year/month/day/hour/minutes/seconds, down the resolution
				// you require.
				// For example the long value 2011021714 would mean February
				// 17, 2011, 2-3 PM.
				doc.add(new LongField("modified", file.lastModified(),
						Field.Store.NO));

				// Add the contents of the file to a field named "contents".
				// Specify a Reader, so that the text of the file is
				// tokenized and
				// indexed, but not stored. Note that FileReader expects the
				// file to be in UTF-8
				// encoding.
				// If that's not the case searching for special characters
				// will fail.

				if (file.getName().substring(file.getName().length() - 4)
						.equals(".htm")) {

					String text = Jsoup.parse(file, null).text();
					// System.out.println(text);
					doc.add(new TextField("contents", text, Store.YES));
				} else {
					Reader reader = new BufferedReader(new InputStreamReader(
							fis, "UTF-8"));

					doc.add(new TextField("contents", reader));
				}

				if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
					// New index, so we just add the document (no old
					// document can be there):
					System.out.println("adding " + file);
					writer.addDocument(doc);
				} else {
					// Existing index (an old copy of this document have
					// been indexed) so we use updateDocument instead to
					// replace the old one matching the exact path, if
					// present:
					System.out.println("updating " + file);
					writer.updateDocument(new Term("path", file.getPath()), doc);
				}

			} finally {
				fis.close();
			}
		}
	}
}
