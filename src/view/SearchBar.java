package view;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

public class SearchBar extends JTextField {

	public SearchBar() {
		super("Search");
		setText("Type Search Query Here and Hit Enter");
		setActionCommand("Search");
		addFocusListener(new SearchBarFocusListener());
	}

	private class SearchBarFocusListener implements FocusListener {

		@Override
		public void focusGained(FocusEvent e) {
			((SearchBar) e.getSource()).selectAll();
		}

		@Override
		public void focusLost(FocusEvent e) {/* Do Nothing */
		}
	}

}
