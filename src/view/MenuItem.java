package view;

import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

public class MenuItem extends JMenuItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MenuItem(String text, ActionListener menuListener) {
		super(text);
		addActionListener(menuListener);
	}

}
