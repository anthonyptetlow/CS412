package view;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import model.IndexModel;

public class AppGUI extends JFrame implements Observer {

	public static String VERSION = "0.2";

	private ResultsPanel resultsPanel;
	private IndexModel model;
	private SearchBar searchBar;

	public AppGUI(IndexModel model, ActionListener menuListener) {
		this.model = model;
		// Build Menu
		buildMenuBar(menuListener);
		// Build Search bar
		buildSearchBar(menuListener);
		// TODO Build Index Button

		// TODO Build Results Page
		buildResultsPanel();
		buildFrame();
	}

	private void buildFrame() {
		Image appIcon = Toolkit.getDefaultToolkit().getImage("Img/search.jpg");
		setIconImage(appIcon);
		setSize(1000, 1000);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void buildMenuBar(ActionListener menuListener) {
		// Menu Bar
		JMenuBar menuBAR = new JMenuBar();
		setJMenuBar(menuBAR);

		// File Menu
		final JMenu file = new JMenu("Menu");
		menuBAR.add(file);
		// TODO Add Index option to file menu
		file.add(new MenuItem("Index", menuListener));
		// Item: CLOSE
		MenuItem close = new MenuItem("Close", menuListener);
		file.add(close);
		final int SHORTCUT_MASK = Toolkit.getDefaultToolkit()
				.getMenuShortcutKeyMask();
		close.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
				SHORTCUT_MASK));
		// HELP
		final JMenuItem help = new JMenu("Help");
		menuBAR.add(help);

		// Menu Help -> About
		JMenuItem about = new MenuItem("About App", menuListener);
		help.add(about);
	}

	private void buildSearchBar(ActionListener searchBarListener) {
		searchBar = new SearchBar();
		searchBar.addActionListener(searchBarListener);
		getContentPane().add(searchBar, BorderLayout.NORTH);
	}

	private void buildResultsPanel() {
		// Extract this TextArea within ScrollPanel Within Panel out to separate
		// class
		resultsPanel = new ResultsPanel();
		add(resultsPanel, BorderLayout.CENTER);
		resultsPanel.setVisible(false);

	}

	@Override
	public void update(Observable o, Object arg) {
		String result = displayResultsStatistics(searchBar.getText(),
				model.getTotalHits());
		result += retriveAndFormatSearchResults();
		resultsPanel.setText(result);
		resultsPanel.setVisible(true);
	}

	public String displayResultsStatistics(String query, int totalHits) {
		return "\n\t\tSEARCHING FOR: [ " + query + " ]"
				+ "\n\n\t\tTOTAL MATCHING DOCUMENTS: " + "[ " + totalHits
				+ " ]\n\n";
	}

	public String retriveAndFormatSearchResults() {
		List<Integer> lineNums = model.getLineNum();
		List<String> paths = model.getPath();
		String results = "";
		for (int i = 0; i < lineNums.size() && i < paths.size(); i++) {
			results += "\t" + lineNums.get(i) + ". " + paths.get(i) + "\n";
		}
		return results;
	}
}
