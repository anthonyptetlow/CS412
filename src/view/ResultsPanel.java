package view;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ResultsPanel extends JPanel {

	private JTextArea results;

	public ResultsPanel() {
		super();

		results = new JTextArea(50, 65);
		JScrollPane scroll = new JScrollPane(results);
		results.setLineWrap(true);
		results.setWrapStyleWord(true);
		results.setEditable(false);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		add(scroll);
	}

	public void append(String s) {
		results.append(s);
	}

	public void setText(String s) {
		results.setText(s);
	}
}
