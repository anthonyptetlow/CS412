package main;

import model.IndexModel;
import view.AppGUI;
import controller.NewController;

public class main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		IndexModel model = new IndexModel();
		NewController controller = new NewController(model);
		model.addObserver(new AppGUI(model, controller));

	}
}
