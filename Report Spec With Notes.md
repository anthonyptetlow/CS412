To be emailed to D. Roussinov by the beginning of week 9
Brief description of possible users (1 paragraph)
		- The users for this system would be mainly programmers looking for information of coding in java. (Expand)

At least three examples of hypothetical user search tasks, possible queries to them and relevant documents in your dataset.
	- Searching For Topics
	- Cross Referening topics
	- 
Query languages supported (e.g. simple list of words, use of “+” or “-“, boolean, AND, NOT, OR, use of phrases, assigning weights to terms, etc.)
	- Single Word Search
	- Boolean Search
	- 
What fields can be searched
	- text Full
	- Titles
	- Headers
	- Paragraphs
Use of stemming
	- We will have a look at Lucenes Capabilities
Removing stopwords
	- Again Lucene
What is displayed (snippet of text, title, date, file name, etc.)? In what format? If any text is highlighted?
If the user can provide relevance feedback
If the system makes any suggestions to the user?
Any other ideas that you are considering implementing to claim as novelty, for example pseudo-relevance feedback, clustering, diversifying search results, query expansions (going beyond bag of words), etc.

